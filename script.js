let book = document.querySelectorAll('.book');
console.log(book);

book[1].after (book[0]);
book[4].after (book[2]);
book[4].after (book[3]);
book[5].after (book[2]);

let bodyImg = document.querySelector('#body');

let oneNameBook = document.querySelector('#nameBook');
let twoNameBook = document.createElement('div');
twoNameBook.textContent='Книга 3. this и Прототипы Объектов';
oneNameBook.replaceWith(twoNameBook);

let adv = document.querySelector('.adv');
adv.remove();

let twoBook = document.querySelectorAll('#twoBook');
console.log(twoBook);
twoBook[3].after (twoBook[2]);
twoBook[2].before (twoBook[6]);
twoBook[2].before (twoBook[8]);
twoBook[10].before (twoBook[2]);

let fiveBook = document.querySelectorAll('#fiveBook');
console.log(fiveBook);
fiveBook[2].before (fiveBook[9]);
fiveBook[2].before (fiveBook[3]);
fiveBook[2].before (fiveBook[4]);
fiveBook[5].before (fiveBook[6]);
fiveBook[5].before (fiveBook[7]);

let bookSix = document.querySelector('#bookSix');
let sixBook = document.querySelectorAll('#sixBook');
let eightNameBook = document.createElement('div');
eightNameBook.textContent='Глава 8: За пределами ES6';
console.log(sixBook);
bookSix.after(eightNameBook);
